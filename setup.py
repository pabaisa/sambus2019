from setuptools import setup, find_packages


def read_uncommented_lines(file_path: str):
    with open(file_path) as f:
        return [l.strip() for l in f.readlines() if not l.startswith('#')]


def requirements() -> list:
    return read_uncommented_lines('requirements.txt')


setup(
    name="sambus2019",
    version="0.1.1",
    description="Driver for Systemair Residential units, for communication via the Modbus RTU protocol.",
    url="https://gitlab.com/pabaisa/sambus2019",
    license='Apache',
    packages=find_packages(where='src'),
    package_dir={"": "src"},
    install_requires=requirements(),
)
