# sambus
Driver for Systemair Residential units, for communication via the Modbus RTU protocol.

I've used a simple USB to RS485 Adapter http://www.dx.com/p/usb-to-rs485-adapter-black-green-296620

Here's the 2019 Systemair Modbus reference list: https://shop.systemair.com/upload/assets/SAVE_MODBUS_VARIABLE_LIST_20190116__REV__29_.PDF

## Requirements
- minimalmodbus (pip package)
- python3.4 or higher

## Installation
`pip3 install .` or `pip3 install -e .`

## Test / use in CLI
`python3 -m sambus2019`
