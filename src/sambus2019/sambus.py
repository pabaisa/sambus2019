import minimalmodbus

minimalmodbus.BAUDRATE = 19200
minimalmodbus.TIMEOUT = 0.5


# NODE: ALL REGISTERS HERE HAVE TO BE THE ORIGINAL REGISTER CODE MINUS ONE


class SAMbus(minimalmodbus.Instrument):
    """Instrument class for System air\Villavent residential units.

    Communicates via Modbus RTU protocol (via RS485), using the :mod:`minimalmodbus`
    Python module. This driver is intended to enable control of the ventilation
    from the command line.
    Args:
        * portname (str): port name
            * examples:
            * OS X: '/dev/tty.usbserial'
            * Linux: '/dev/ttyUSB0'
            * Windows: '/com3'

        * slaveaddress (int): slave address in the range 1 to 247 (in decimal)"""

    def __init__(self, portname, slaveadress):
        minimalmodbus.Instrument.__init__(self, portname, slaveadress)

    ## Registers for airflow control

    def get_fan_saf_current_rpm(self):
        # REG_SENSOR_RPM_SAF
        return self.read_register(12401 - 1)

    def get_fan_eaf_current_rpm(self):
        # REG_SENSOR_RPM_EAF
        return self.read_register(12402 - 1)

    def get_fan_saf_percentage(self):
        # REG_OUTPUT_SAF
        return self.read_register(14001 - 1)

    def get_fan_eaf_percentage(self):
        # REG_OUTPUT_EAF
        return self.read_register(14002 - 1)

    ## Registers for other

    def get_rh_sensor(self):
        # REG_DEMC_RH_PI_FEEDBACK
        return self.read_register(1012 - 1)

    def get_rh_demand(self):
        # REG_DEMC_RH_PI_SP
        return self.read_register(1011 - 1)

    def get_iaq_level(self):
        iaq_levels = {
            0: "Economic",
            1: "Good",
            2: "Improving",
        }
        # REG_IAQ_LEVEL
        return iaq_levels[self.read_register(1123 - 1)]

    def get_usermode(self):
        usermode_levels = {
            0: "Auto",
            1: "Manual",
            2: "Crowded",
            3: "Refresh",
            4: "Fireplace",
            5: "Away",
            6: "Holiday",
            7: "Cooker Hood",
            8: "Vacuum Cleaner",
            9: "CDI1",
            10: "CDI2",
            11: "CDI3",
            12: "PressureGuard",
        }
        # REG_USERMODE_MODE# REG_USERMODE_MODE
        return usermode_levels[self.read_register(1161 - 1)]

    def set_refresh(self, time):
        self.write_register(1104 - 1, time)  # REG_USERMODE_REFRESH_TIME
        return self.write_register(1162 - 1, 4)  # REG_USERMODE_HMI_CHANGE_REQUEST

    def get_usermode_remaining(self):
        # REG_USERMODE_REMAINING_TIME_L and REG_USERMODE_REMAINING_TIME_H
        return self.read_long(
            1111 - 1
        )

    def get_filter_remaining(self):
        # REG_FILTER_REMAINING_TIME_L and REG_FILTER_REMAINING_TIME_H
        return self.read_long(
            7005 - 1
        )

    ## Registers for heater

    def get_triac_active(self):
        triac_status = {
            1: "Active",
            0: "Inactive",
        }
        # REG_OUTPUT_TRIAC
        return triac_status[self.read_register(14381 - 1)]

    def get_triac_voltage(self):
        # REG_PWM_TRIAC_OUTPUT
        return self.read_register(
            2149 - 1,
            number_of_decimals=1,
        )
    def get_heater_active(self):
        heater_status = {
            1: "Active",
            0: "Inactive",
        }
        # REG_OUTPUT_Y1_DIGITAL
        return heater_status[self.read_register(14102 - 1)]

    def get_heater_voltage(self):
        # REG_OUTPUT_Y1_ANALOG
        return self.read_register(
            14102 - 1,
            number_of_decimals=1,
        )

    def get_heatexchanger_active(self):
        heater_status = {
            1: "Active",
            0: "Inactive",
        }
        # REG_OUTPUT_Y2_DIGITAL
        return heater_status[self.read_register(14104 - 1)]

    def get_heatexchanger_voltage(self):
        # REG_OUTPUT_Y2_ANALOG
        return self.read_register(
            14103 - 1,
            number_of_decimals=1,
        )

    ## Registers for temperature

    def get_temp_mode(self):
        temp_modes = {
            0: "Supply",
            1: "Room",
            2: "Extract",
        }
        # REG_TC_CONTROL_MODE
        return temp_modes[self.read_register(2031 - 1)]

    def get_eco_enabled(self):
        eco_modes = {
            0: "Disabled",
            1: "Enabled",
        }
        # REG_ECO_MODE_ON_OFF
        return eco_modes[self.read_register(2505 - 1)]

    def get_eco_active(self):
        eco_modes = {
            0: "Inactive",
            1: "Active",
        }
        # REG_ECO_FUNCTION_ACTIVE
        return eco_modes[self.read_register(2506 - 1)]

    def get_freecooling_enabled(self):
        modes = {
            0: "Disabled",
            1: "Enabled",
        }
        # REG_FREE_COOLING_ON_OFF
        return modes[self.read_register(4101 - 1)]

    def get_freecooling_active(self):
        modes = {
            0: "Inactive",
            1: "Active",
        }
        # REG_FREE_COOLING_ACTIVE
        return modes[self.read_register(4111 - 1)]

    def get_temp_sensor_oat(self):
        # REG_SENSOR_OAT
        return self.read_register(
            12102 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_sensor_sat(self):
        # REG_SENSOR_SAT
        return self.read_register(
            12103 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_sensor_eat(self):
        # REG_SENSOR_PDM_EAT_VALUE
        return self.read_register(
            12544 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_sensor_oht(self):
        # REG_SENSOR_OHT
        return self.read_register(
            12108 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_room(self):
        # REG_TC_EAT_RAT_SP
        return self.read_register(
            2051 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_demand(self):
        # REG_SATC_HEAT_DEMAND
        return self.read_register(2055 - 1)

    def get_temp_target(self):
        # REG_TC_SP
        return self.read_register(
            2001 - 1,
            number_of_decimals=1,
            signed=True,
        )

    def get_temp_target_supply(self):
        # REG_TC_SP_SATC
        return self.read_register(
            2054 - 1,
            number_of_decimals=1,
            signed=True,
        )
