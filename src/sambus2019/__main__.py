from datetime import timedelta

import minimalmodbus

from .sambus import SAMbus

########################
## Testing the module ##
########################


if __name__ == "__main__":
    """
    Example of usage: (reminder)
    import sambus
    inst = sambus.SAMbus('/dev/ttyUSB0', 1)
    inst.set_fan(3)
    """

    print("TESTING SYSTEMAIR MODBUS MODULE")

    PORTNAME = "/dev/ttyUSB0"
    ADDRESS = 1

    print("Port: " + str(PORTNAME) + ", Address: " + str(ADDRESS))
    instr = SAMbus(PORTNAME, ADDRESS)
    instr.serial.timeout = minimalmodbus.TIMEOUT
    instr.serial.baudrate = minimalmodbus.BAUDRATE
    print(instr)

    print()
    print("User mode            :   {0}".format(instr.get_usermode()))
    print()
    print("ECO mode             :   {0}".format(instr.get_eco_enabled()))
    print("ECO mode active      :   {0}".format(instr.get_eco_active()))
    print()
    print("Heat exchanger active:   {0}".format(instr.get_heatexchanger_active()))
    print("Heat exchanger voltag:   {0} V".format(instr.get_heatexchanger_voltage()))
    print()
    print("Heater active        :   {0}".format(instr.get_heater_active()))
    print("Heater voltage       :   {0} V".format(instr.get_heater_voltage()))
    print()
    print("Free Cooling enabled :   {0}".format(instr.get_freecooling_enabled()))
    print("Free Cooling active  :   {0}".format(instr.get_freecooling_active()))
    print()
    print("Temp mode            :   {0}".format(instr.get_temp_mode()))
    print("Temp room (auto)     :   {0} °C".format(instr.get_temp_room()))
    print("Temp demand %        :   {0} %".format(instr.get_temp_demand()))
    print("Temp target          :   {0} °C".format(instr.get_temp_target()))
    print("Temp target supply   :   {0} °C".format(instr.get_temp_target_supply()))
    print()
    print("SAF speed RPM        :   {0} RPM".format(instr.get_fan_saf_current_rpm()))
    print("EAF speed RPM        :   {0} RPM".format(instr.get_fan_eaf_current_rpm()))
    print("SAF speed %          :   {0} %".format(instr.get_fan_saf_percentage()))
    print("EAF speed %          :   {0} %".format(instr.get_fan_eaf_percentage()))
    print()
    print("RH sensor            :   {0} %".format(instr.get_rh_sensor()))
    print("RH demand            :   {0} %".format(instr.get_rh_demand()))
    print()
    print("IAQ level            :   {0}".format(instr.get_iaq_level()))
    print(
        "Air filter remaining :   {0}".format(
            str(timedelta(seconds=instr.get_filter_remaining()))
        )
    )
    print()
    print("OAT sensor           :   {0} °C".format(instr.get_temp_sensor_oat()))
    print("OHT sensor           :   {0} °C".format(instr.get_temp_sensor_oht()))
    print("SAT sensor           :   {0} °C".format(instr.get_temp_sensor_sat()))
    print("EAT sensor           :   {0} °C".format(instr.get_temp_sensor_eat()))
    print()
    print("DONE!")
